<?php
class Game extends Standard {
	static public $dbName = "bot";
	static public $tableName = "Spiel";
	static public $primaryKey = "Spiel_ID";

	public $Spiel_ID;
	public $Save;
	public $SpielArt;
	public $Spieler_1;
	public $Spieler_2;
	public $AmZug;
	static protected $columns = null;
	
	public $Spielfeld = array(array());
	public $Spielsteine;
	public $SpielfeldSize;
	public $SpielfeldFiller;
	
	static public $Games = array(
			"TICTACTOE" => "ttt"
	);
	public function init( $data = null)
	{
		parent::init($data);
		
		$Spielstand = JSON_decode($this->Save, true);
		$this->Spielsteine = $Spielstand["Spielsteine"];
		$this->SpielfeldSize = $Spielstand["SpielfeldSize"];
		$this->SpielfeldFiller = $Spielstand["SpielfeldFiller"];
		$this->Spielfeld = $Spielstand["Spielfeld"];
	}
	public function exist( $name, $consignor )
	{
		$name = strtoupper( $name );
		if( array_key_exists( $name, Game::$Games ) )
		{
			return true;
		}else{
			Nachricht::sendFromBot($consignor->User_ID, "Dieses Spiel existiert nicht");
			return false;
		}
	}
	
	public function runningGame( $name, $consignor )
	{
		if( $game = Game::find("SpielArt ='" . $name . "' AND( Spieler_1='" . $consignor->User_ID . "' OR Spieler_2='" . $consignor->User_ID . "')") )
		{
			return array_pop($game);
		}else{
			$newGame = new Game;
			$newGame->Spieler_1 = $consignor->User_ID;
			$newGame->SpielArt = $name;
			return $newGame;
		}
	}
	public function pendingGameTurn( $consignor )
	{
		if( $game = Game::find("AmZug='" . $consignor->User_ID . "'") )
		{
			return array_pop($game);
		}else{
			return false;
		}
	}
	public function startGame()
	{
		$this->save();
		$AmZug = $this->getStartingPlayer();
		
		switch( $this->SpielArt )
		{
			case "TICTACTOE":
				$this->SpielfeldFiller = "\xE2\xAC\x9C";
				$this->SpielfeldSize = 3;
				$this->Spielsteine = array(
						$this->Spieler_1 => "\xE2\xAD\x95",
						$this->Spieler_2 => "\xE2\x9D\x8C"
				);
				for($y = 0;$y < 3; $y++)
				{
					for($x = 0;$x < 3; $x++)
					{
						$this->Spielfeld[$y][$x] = $this->SpielfeldFiller;
					}
				}
				$json = JSON_encode(array(
						"SpielfeldFiller" => $this->SpielfeldFiller,
						"Spielfeld" => $this->Spielfeld,
						"Spielsteine" => $this->Spielsteine,
						"SpielfeldSize"=>$this->SpielfeldSize
				));
				$this->Save = $json;
				$this->Save();
			break;
		}

		$this->printGame($AmZug);
	}
	public function printGame( $User_ID = false )
	{
		
		$spieler1 = User::findUser( $this->Spieler_1 );
		$spieler2 = User::findUser( $this->Spieler_2 );
		$AmZug = User::findUser( $this->AmZug );
		$msg = "Spielsteine:\n$spieler1->Nickname = ".$this->Spielsteine[$spieler1->User_ID]."\n";
		$msg .= "$spieler2->Nickname = ".$this->Spielsteine[$spieler2->User_ID]."\n---------------------------\n";
		for($y = 0;$y <= $this->SpielfeldSize; $y++)
		{
			for($x = 0;$x <= $this->SpielfeldSize; $x++)
			{
				if($x  == 0 && $y == 0)
				{
					$msg .= "\t\t\t\t ";
				}
				if($x == 0 && $y > 0)
				{
					$msg .= "\t\tY$y\t";
				}
				if($y == 0 && $x > 0)
				{
					$msg .= "X$x ";
				}
				if($y > 0 && $x > 0)
				{
					$msg .= $this->Spielfeld[$y-1][$x-1];
				}
				if($x == $this->SpielfeldSize -1)
				{
				//	$msg .= "|";
				}
			}
			$msg .= "\n";
		}
		$msg .= "---------------------------\n";
		$msg .= $AmZug->Nickname . " ist am Zug";
		$msg .= "\n---------------------------\n";
		$msg .= "Antworte z.B. mit: x1,y3";
		if( $User_ID ) $AmZug->User_ID = $User_ID;
		Nachricht::sendFromBot($AmZug->User_ID, $msg);
	
	}
	public function answerGame( $nachricht )
	{
		switch($this->SpielArt)
		{
			case "TICTACTOE":
				$nachricht = str_replace(" ", "", $nachricht);
				$message_exploded = explode(" ", $nachricht);
				$befehl = $message_exploded[0];
				$befehl = str_replace(".", ",", $befehl);
				$befehl = explode(",", $befehl);
				$x = null;
				$y = null;
				foreach($befehl as $b)
				{
					$axis = substr($b, 0, 1);
					switch(strtoupper($axis))
					{
						case "X":
							$x = (int)substr($b, 1, 1);
							break;
						case "Y":
							$y = (int)substr($b, 1, 1);
							break;
					}
				}
				if(is_null($x) || is_null($y))
				{
					Nachricht::sendFromBot($this->AmZug, "Ungültige Antwort");
					return false;
				}else{
					if($x <= 0 || $x > $this->SpielfeldSize || $y <= 0 || $y > $this->SpielfeldSize)
					{
						Nachricht::sendFromBot($this->AmZug, "Spielstein wäre außerhalb des Spielfelds.");
						return false;
					}
					if($this->Spielfeld[$y-1][$x-1] == $this->SpielfeldFiller)
					{
						$actualSpieler = $this->AmZug;
						$this->changePlayer();
						$this->Spielfeld[$y-1][$x-1] = $this->Spielsteine[$actualSpieler];
						Nachricht::sendFromBot($actualSpieler, "Dein Gegenspieler ist nun am Zug.");
						$this->saveSpielstand();
						$this->printGame();
						usleep(55000);
						$this->printGame( $actualSpieler );
						if( !$this->checkWin() )
						{
							$this->checkDraw();
						}
						
					}else{
						Nachricht::sendFromBot($this->AmZug, "Spielfeld bereits belegt.");
						return false;
					}
				}
			break;
		}
	}
	private function checkWin()
	{
		switch($this->SpielArt)
		{
			case "TICTACTOE":
			
				/*Horizontal*/
				$won = false;
				$horizontal = 0;
				$vertikal = 0;
				$winningString = "";
				$variants = array();
				$count = 0;
				foreach($this->Spielfeld as $y)
				{
					$variants[] = $y;
					$variants[] =[
							$this->Spielfeld[0][$count],
							$this->Spielfeld[1][$count],
							$this->Spielfeld[2][$count]
					];
					$count++;
				}
				$variants[] = [$this->Spielfeld[0][0], $this->Spielfeld[1][1], $this->Spielfeld[2][2]];  /* DIAGONAL */
				$variants[] = [$this->Spielfeld[2][0], $this->Spielfeld[1][1], $this->Spielfeld[0][2]];	/* DIAGONAL */
				
				foreach($variants as $v)
				{
					$amount = array_count_values($v);
					foreach($amount as $string=>$a)
					{
						if($a == $this->SpielfeldSize && $string != $this->SpielfeldFiller)
						{
							$winningString = $string;
							$won = true;
						}
					}
				}
				if($won)
				{
					$this->win( $winningString );	
					return true;
				}
				return false;
			break;
		}
	}
	private function checkDraw()
	{
		switch($this->SpielArt)
		{
			case "TICTACTOE":
				$draw = true;
				for($y = 0;$y < $this->SpielfeldSize;$y++)
				{
					for($x = 0;$x < $this->SpielfeldSize;$x++)
					{
						if($this->Spielfeld[$y][$x] == $this->SpielfeldFiller)
						{
							$draw = false;
							return true;
						}
					}
				}
				if($draw)
				{
					foreach($this->Spielsteine as $spieler=>$stein)
					{
						Nachricht::sendFromBot($spieler, "Spiel unentschieden!");
						usleep(55000);
					}
					$this->delete("Spiel_ID='". $this->Spiel_ID ."'");
				}
			break;
		}
	}
	private function win( $winningString )
	{
		foreach($this->Spielsteine as $spieler=>$stein)
		{
			if($stein == $winningString)
			{
				$winner = $spieler;
				$winStein = $stein;
			}
		}
		$winner = User::findUser( $winner );
		foreach($this->Spielsteine as $spieler=>$stein)
		{
			Nachricht::sendFromBot($spieler, "$winner->Nickname($winStein) hat das Spiel gewonnen!");
			usleep(55000);
		}
		$this->delete("Spiel_ID='". $this->Spiel_ID ."'");
	}
	private function changePlayer()
	{
		$spieler = array(
				$this->Spieler_1,
				$this->Spieler_2
		);
		foreach($spieler as $s)
		{
			if($s != $this->AmZug)
			{
				trigger_error($s);
				$this->AmZug = $s;
				return true;
			}
		}
	}
	private function saveSpielstand()
	{
		$json = JSON_encode(array(
				"SpielfeldFiller" => $this->SpielfeldFiller,
				"Spielfeld" => $this->Spielfeld,
				"Spielsteine" => $this->Spielsteine,
				"SpielfeldSize"=>$this->SpielfeldSize
		));
		$this->Save = $json;
		$this->Save();
	}
	private function getStartingPlayer() 
	{
		$spieler = [$this->Spieler_1, $this->Spieler_2];
		shuffle( $spieler );
		$index = rand( 0, 1 );
		$AmZug = $spieler[$index];
		$this->AmZug = $AmZug;
		$this->save();
		
		foreach($spieler as $s)
		{
			if($s == $AmZug)
			{
				Nachricht::sendFromBot($AmZug, "TICTACTOE, das Spiel beginnt.\nSie sind am Zug");
			}else{
				Nachricht::sendFromBot($s, "TICTACTOE, das Spiel beginnt.\nIhr Gegenspieler ist am Zug");
			}
			usleep(55000);
		}
		
		return $AmZug;
		
	}
}
?>