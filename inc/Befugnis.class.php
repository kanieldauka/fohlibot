<?php
class Befugnis extends Standard{
	static public $dbName = "bot";
	static public $tableName = "Rights";
	static public $primaryKey = "Rights_ID";

	public $Rights_ID;
	public $User_ID;
	public $Befugnis;
	public $Date;
	static protected $columns = null;
	

	static public $defaultFunctions = ["HELP", "HELP <befehl>", "REGISTER", "TIMETABLE"];
	static public $addableRights = ["TEMP", "CHANGECLASS", "PLAY", "MASTER", "SENDMESSAGE", "SETNICKNAME", "SHOWUSERS", "NOTIFY", "REMOVERIGHT", "FORCENICKNAME", "FORCEREGISTER", "SHUTDOWN", "REBOOT", "ADDRIGHT"];
	static public $befehlsInfo = array(
			"REGISTER" => "Register benötigt keine zusätzlichen eingaben. Einfach @register schreiben und los gehts.",
			"HELP" => "@HELP help .... ernsthaft?",
			"ADDRIGHT" => "Fügt Rechte einer Person hinzu\n------------\n@ADDRIGHT <telefonnummer> <befugnis>\nBeispiel:@ADDRIGHT 4917639965973 MASTER\nFügt dem User 4917639965973 die Befugnis zum ausführen des Befehls MASTER hinzu",
			"MASTER" => "hmm",
			"SENDMESSAGE" => "Verschickt eine Nachricht als Fohlibot\n------------\n@SENDMESSAGE <telefonnummer> <nachricht>\nBeispiel:@sendmessage 4917639965973 Hallo Welt",
			"SETNICKNAME" => "Ändert deinen Nicknamen\n------------\n@SETNICKNAME <nickname>",
			"SHOWUSERS" => "Zeigt alle Benutzer an",
			"TIMETABLE" => "Zeigt den aktuellen Stundenplan an",
			"NOTIFY" => "Versendet durch Events getriggert Nachrichten.\n------------\nBeispiel:\n@notify timetable\nSendet bei veränderungen im Stundenplan eine Nachricht",
			"REMOVERIGHT" => "Entfernt dem Ziel ein bestimmtes Recht\n------------\nBeispiel:\n@removeright Johannes sendmessage",
			"FORCENICKNAME" => "@forcenickname <ziel> <befugnis>",
			"FORCEREGISTER" => "@forceregister <zieltelefonnumemr> <nickname>",
			"SHUTDOWN" => "Fährt den Fohlibot Server herunter",
			"REBOOT" => "Startet den Fohlibot Server neu",
			"PLAY" => "Startet ein spiel mit dem Ziel\n------------\nBeispiel:\n@play johannes tictactoe",
			"CHANGECLASS" => "Wechselt die Klasse\n------------\nBeispiel:\n@changeclass fia51"
	);
	
	static public function isBefugnis( $command ) {
		if(in_array(substr($command, 0, 1), array("@", "$"))) // beginnt Befehl mit @ oder $ ?
		{
			$command = substr($command, 1, strlen($command)-1);
			$allCommands = array_merge(static::$defaultFunctions, static::$addableRights);
			return in_array(strtoupper( $command ), $allCommands);
		}else{
			return false;
		}
	}
	static public function isSilent( $command ) {
		return false;
	}
	public function runCommand( $consignor, $msg) {
		$message_exploded = explode(" ", $msg);
		$befehl = strtoupper(str_replace( array("@", "$"), array("", ""), $message_exploded[0])); 	//@ oder $ entfernen
		$silent = Befugnis::isSilent( $befehl ); 	//ist Befehl silent? ( keine antworten vom Bot )
		
		switch( $befehl ){
			case "REGISTER":
				
				if(empty($consignor->ID)) {
					$consignor->setDefaultRights();
				
					$msg_header = "Hallo $consignor->Nickname,\ndu wurdest erfolgreich registriert.\nDeine aktuellen erlaubten Befehle sind:";
					$msg_body = "";
					$rechte = $defaultFunctions;
					array_merge(static::$defaultFunctions, $user->Rights);
					foreach($user->Rights as $befugnis) {
						$rechte[] = $befugnis->Befugnis;
					}
					foreach($rechte as $befugnis) {
						if($befugnis != "REGISTER") {
							$msg_body .= "\n\t\xE2\x9C\x94@".$befugnis;
						}
					}
				
					$consignor->save();
					Nachricht::sendFromBot($consignor->User_ID, $msg_header . $msg_body);
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Du bist bereits registriert \ngib @HELP ein um alle verfügbaren Befehle zu sehen");
				}
			break;
			
			case "HELP":
				if(count($message_exploded)>1) 	// help MIT Parameter
				{ 
					$lookUpFor = strtoupper(str_replace("@", "", $message_exploded[1]));
				
					if(array_key_exists($lookUpFor, self::$befehlsInfo)) {
						$sendMsg = self::$befehlsInfo[$lookUpFor];
					}else{
						$sendMsg = "Konnte Befehl $lookUpFor nicht finden.";
					}
					Nachricht::sendFromBot($consignor->User_ID, $sendMsg);
				}else{		//help OHNE Parameter
					if($consignor->ID) {
						$rechte = array_merge(self::$defaultFunctions, $consignor->Rights);
					}else{
						$rechte = $defaultFunctions;
					}
					$sendMsg = "Deine ausführbaren Befehle sind:";
					foreach($rechte as $befehl) {
							
						if($befehl === "REGISTER") {
							if(!$consignor->ID) {
								$sendMsg .= "\n\t@REGISTER";
							}
						}else{
							$sendMsg .= "\n\t@" . $befehl;
						}
					}
					Nachricht::sendFromBot($consignor->User_ID, $sendMsg);
				}
			break;
			
			case "SHOWUSERS":
				$allUser = User::find("1", "Class ASC");
				$msg = "Benutzer:\n--------------------------------------";
				$class = "";
				foreach($allUser as $u) {
					if($u->Class != $class)
					{
						$class = $u->Class;
						$msg .= "\n";
						if( strpos($class, "AVM") !== false)
						{
							$sign = "\xF0\x9F\x8E\xA5";
						}else if( strpos($class, "FIA") !== false ) 
						{
							$sign = "\xF0\x9F\x92\xBB";
						}
						$msg .= $sign . " ===== Klasse: $u->Class";
					}
					$msg .= "\n$sign\t" . $u->Nickname . ":$u->User_ID";
				}
				Nachricht::sendFromBot($consignor->User_ID, $msg);
			break;
			
			case "SENDMESSAGE":
				$target_user = User::find("Nickname='".$message_exploded[1]."'");
				if($target_user)
				{
					$target_user = $target_user[0]->User_ID;
				}else if(is_numeric($message_exploded[1]))
				{
					$target_user = $message_exploded[1];
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Kein gültiger Empfänger");
					return false;
				}
								
				$msg = "";
				for($i=2;$i<count($message_exploded);$i++) {  //fügt die Nachrichtenparameter zusammen
					$msg .= $message_exploded[$i];
					$msg .= " ";
				}
					Nachricht::sendFromBot($target_user, $msg);
				break;
				
			case "ADDRIGHT":
				$right = strtoupper($message_exploded[2]);  									//Befugnis das hinzugefügt werden soll
				if($target_user = $consignor->findUser( $message_exploded[1] )) 
				{
					if(in_array($right, self::$addableRights))  //ist auch ein hinzufügbares Befugnis ??
					{
						if(!in_array($right, $target_user->Rights))	//Zieluser hat Befugnis nicht?
						{
							$befugnis = new Befugnis;
							$befugnis->User_ID = $target_user->User_ID;		//Befugnis speichern
							$befugnis->Befugnis = $right;
							$befugnis->save();
								
							Nachricht::sendFromBot($consignor->User_ID, "Befugnis hinzugefügt.");	//Nachricht an den Ausführer
							
							usleep(550000);			//nötig für whatsapp Api, halbe sekunde delay
							
							Nachricht::sendFromBot($target_user->User_ID, "Ein Pferdeflüsterer hat dir den Befehl @" . $message_exploded[2] . " hinzugefügt.\nFür weitere infos gib:\n@help $message_exploded[2]\nein."); //Nachricht an das Ziel
							
						}else{
							Nachricht::sendFromBot($consignor->User_ID, "Zieluser hat das Befugnis bereits.");
						}
					}else{
						Nachricht::sendFromBot($consignor->User_ID, "$right ist kein hinzufügbarer Befehl.");
					}
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Konnte Empfänger nicht finden");
				}
				break;
				
			case "REMOVERIGHT":
				$right = strtoupper($message_exploded[2]);  									//Befugnis das hinzugefügt werden soll
				if( $target_user = $consignor->findUser( $message_exploded[1]) )	//kann Ziel gefunden werden?
				{
					if(in_array($right, self::$addableRights))  //ist auch ein hinzufügbares Befugnis ??
					{
						if(in_array($right, $target_user->Rights))	//Zieluser hat Befugnis ?
						{
							$befugnis = new Befugnis;
							$befugnis->delete("User_ID=$target_user->User_ID and Befugnis='".$right."'");
							Nachricht::sendFromBot($consignor->User_ID, "$right wurde dem Ziel entzogen.");
							usleep(550000);
							Nachricht::sendFromBot($target_user->User_ID, "Ein Pferdeflüsterer hat dir den Befehl @" . $right . " weggenommen");
							
						}else{
							Nachricht::sendFromBot($consignor->User_ID, "Zieluser hat das Befugnis nicht.");
						}
					}else{
						Nachricht::sendFromBot($consignor->User_ID, "$right ist kein hinzufügbarer Befehl.");
					}
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Konnte Empfänger nicht finden");
				}
			break;
			
			case "SETNICKNAME":
				$Nickname = $message_exploded[1];
				$otherUser = User::find("Nickname='".$Nickname."'");
				
				if($otherUser) {
					Nachricht::sendFromBot($consignor->User_ID, "Der Nickname $Nickname ist bereits vergeben");
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Dein neuer Nickname ist nun $Nickname.");
					$consignor->Nickname = $Nickname;
					$consignor->save();
				}
			break;
			
			case "TIMETABLE":
				
				if($consignor->Class)
				{
					$stunden = Stunde::find("Class='". $consignor->Class . "'", "date ASC, startTime ASC");
					$msg = "";
					$stun = 0;
					$lastDate = "";
					foreach($stunden as $stunde) {
						$date = date("d.m.y --- D", strtotime($stunde->date));
						if($date != $lastDate) {
							$lastDate = $date;
							$msg .="\n\t=== $date ===\n";
							$msg .="-----------------------------------------------\n";
						}
						$msg .= ("$stunde->finished\t" . substr($stunde->startTime, 0, 2).":".substr($stunde->startTime, 2, 2) /* ."-". substr($stunde->endTime, 0, 2).":".substr($stunde->endTime, 2, 2)*/);
						$msg .= "\t ". $stunde->eigenschaften['3']->name . " | " . $stunde->eigenschaften['2']->name . " | " . (($stunde->roomSubstitution) ? "\xE2\x9A\xA0".$stunde->eigenschaften['4']->name : $stunde->eigenschaften['4']->name) ."\n";
						$stun = $stun + 1;
						if(substr($stunde->startTime, 3, 1) == "0") {
							$stun = 0;
							//$msg .=($stunde->Cancelled) ? "\xF0\x9F\x9A\xAB" : "";
							$msg .="-----------------------------------------------\n";
						}
					}
					Nachricht::sendFromBot($consignor->User_ID, $msg);
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Du musst in einer Klasse sein.\nBenutze @changeclass");
				}

			break;
			
			case "FORCENICKNAME":
				if($Nickname = $message_exploded[2]) { //Nickname als zweiter Parameter angegeben?
					if( $target_user = $consignor->findUser( $message_exploded[1] ) )	//kann Ziel finden? 
					{
						$target_user->Nickname = $Nickname;
						$target_user->save();
						if(!$silent) {
							Nachricht::sendFromBot($target_user->User_ID, "Dein Nickname wurde von einem Pferdeflüsterer in $Nickname geändert.");
						}
					}else{
						Nachricht::sendFromBot($consignor->User_ID, "Konnte Empfänger nicht finden");
					}
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Du musst einen Nicknamen als zweiten Parameter eingeben");
				}
			break;
			
			case "NOTIFY":
				$notify_list = ["TIMETABLE"];
				$param = $message_exploded[1];
				if(in_array(strtoupper($param), $notify_list)) //ist zweiter Parameter auch korrekt?
				{  
					if($consignor->addNotice($param)) {
						Nachricht::sendFromBot($consignor->User_ID, "Du wirst nun über Änderungen im $param informiert.");
					}else{
						Nachricht::sendFromBot($consignor->User_ID, "Du wirst nun nicht mehr über Änderungen im $param informiert.");
					}
				}else{
					$msg = "$param ist kein gültiger Parameter für @NOTIFY.\nGültige Parameter:";
					foreach($notify_list as $notice) {
						$msg .= "\n$notice";
					}
					Nachricht::sendFromBot($consignor->User_ID, $msg);
				}
			break;
			
			case "FORCEREGISTER":
				if( $Nickname = $message_exploded[2] ) //Nickname als zweiten Parameter angegeben?
				{
					if(!$target_user = User::find("User_ID='".$message_exploded[1]."'")[0])  //Ziel ist nicht registriert?
					{
						if(is_numeric($message_exploded[1])) {
							$newUser = new User;
							$newUser->User_ID = $message_exploded[1];
							$newUser->Nickname = $Nickname;
							$newUser->setDefaultRights();
							$newUser->save();
							$newUser->setDefaultRights();
							if(!$silent) {
								Nachricht::sendFromBot($newUser->User_ID, "Du wurdest mit dem Nicknamen ". $message_exploded[2] ." Registriert.Schreibe @help für deine ausführbaren Befehle");
							}
					
						}else{
							if(!$silent) {
								Nachricht::sendFromBot($consignor->User_ID, "Konnte Ziel nicht finden. Bitte Telefonnummer eingeben.\n@forceregister 49176......");
							}
						}
					}else{
						if(!$silent) {
							Nachricht::sendFromBot($consignor->User_ID, "Ziel ist bereits registriert");
						}
					}
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Als Zweiten Parameter den Nicknamen angeben");
				}
			break;
			
			case "SHUTDOWN":
				Nachricht::sendFromBot($consignor->User_ID, "Ich bin dann mal weg!");
				$file = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "shutDown.bat";
				exec( $file );
			break;
			
			case "REBOOT":
				Nachricht::sendFromBot($consignor->User_ID, "Fohli sagt bis gleich!");
				shell_exec('sudo reboot');
			break;
			
			case "PLAY":
				$userToFind = $message_exploded[1];
				$gameName = strtoupper($message_exploded[2]);
				
				if( $target_user = $consignor->findUser( $userToFind ) ) //ziel existiert?
				{
					if( Game::exist( $gameName, $consignor ) )	//eingegebenes Spiele existiert?
					{
						if( $game = Game::runningGame( $gameName, $consignor) ) //spielstand vom laufendem Spiel oder gibt neues spiel zurück
						{
							if( !$game->Spiel_ID )	//neues spiel?	
							{
								
								$game->save();		//neues spiel eröffnen und spiel_ID erzeugen
								$consignor->invite($target_user->User_ID, $game);
							}else{
								if(!$game->Spieler_2) //pending invitation
								{
									Nachricht::sendFromBot($consignor->User_ID, "Die Einladung zu diesem Spiel steht noch offen");
								}else{
									$game->printGame( $consignor->User_ID );
								}
							}
						}
					}
				}
			break;
			
			case "CHANGECLASS":
				$class = $message_exploded[1];
				if(array_key_exists(strtoupper($class), Stundenplan::$SupportedClasses))
				{
					$consignor->Class = strtoupper( $class );
					$consignor->Save();
					Nachricht::sendFromBot($consignor->User_ID, "Klasse gewechselt");
				}else{
					Nachricht::sendFromBot($consignor->User_ID, "Klasse ($class) existiert nicht");
				}
				
			break;
			
			case "TEMP":
				$measurements = "";
				for($count = 0; $count < 5; $count++)
				{
					$measurements .=($count+1) . " : " . shell_exec('vcgencmd measure_temp');
					sleep(1);
				}
				Nachricht::sendFromBot($consignor->User_ID, $measurements);
			break;
		}
	}
}
?>
