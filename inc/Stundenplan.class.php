<?php
class Stundenplan {
	static public $SupportedClasses = array( "FIA51" => 391, "AVM32" => 350 , "FIS54" => 413);
	
	static public function updateIfNeeded() {
		foreach(static::$SupportedClasses as $class=>$ID)
		{
			$path = dirname(dirname(__FILE__))."/stundenplan/";
			$json_current = file_get_contents( $path . $class . $_ENV["STUNDENPLAN_CURRENT"] );
			$json_update = file_get_contents( $path . $class . $_ENV["STUNDENPLAN_UPDATE"] );
			
			$stundenplan_current = JSON_decode( $json_current , true);
			$stundenplan_update = JSON_decode( $json_update , true);
			
			$timestamp_update = $stundenplan_update["result"]["lastImportTimestamp"];
			$timestamp_current = $stundenplan_current["result"]["lastImportTimestamp"];
			
			if( ( $timestamp_current && $timestamp_update ) && $timestamp_current < $timestamp_update  )
			{
				file_put_contents( $path . $class . $_ENV["STUNDENPLAN_CURRENT"], $json_update);
				
				/*		RESET OLD DATA		*/
				$reset = new Eigenschaft; 
				$reset->delete("Class='" . $class ."'");
				$reset = new Stunde;
				$reset->delete("Class='" . $class ."'");
				$reset = new Stundeneigenschaften;
				$reset->delete("Class='" . $class ."'");
	
				
				/*		Eigenschaften speichern		*/
				$eigenschaften = $stundenplan_update["result"]["data"]["elements"];
				foreach($eigenschaften as $e)
				{
					$eigenschaft = new Eigenschaft;
					$eigenschaft->apply($e);
					$eigenschaft->Class = $class;
					$eigenschaft->save();
				}
				
				/*	4915161434444	Stunden speichern		*/
				$stunden = $stundenplan_update["result"]["data"]["elementPeriods"][$ID];
				foreach($stunden as $s)
				{
					$s["Class"] = $class;
					$s["startTime"] = str_pad($s["startTime"], 4, "0", STR_PAD_LEFT);
					$s["endTime"] = str_pad($s["endTime"], 4, "0", STR_PAD_LEFT);
					if(!$s["hasInfo"]) {
						$s["hasInfo"] = 0;
					}
					$stunde = new Stunde;
					if(array_key_exists("standard", $s["is"])) {
						$s["Standard"] = 1;
					}
					if(array_key_exists("cancelled", $s["is"])) {
						$s["Cancelled"] = 1;
					}
					if(array_key_exists("roomSubstitution", $s["is"])) {
						$s["roomSubstitution"] = 1;
					}
					if(array_key_exists("substitution", $s["is"])) {
						$s["substitution"] = 1;
					}
				
					$stunde->apply($s);
					$stunde->Save();
					foreach($s["elements"] as $eigenschaft) {
						$stundeneigenschaften = new Stundeneigenschaften;
						$stundeneigenschaften->Class = $class;
						$stundeneigenschaften->S_ID = $stunde->id;
						$stundeneigenschaften->E_ID = $eigenschaft["id"];
						$stundeneigenschaften->type = $eigenschaft["type"];
						$stundeneigenschaften->Save();
					}
				}
				//Nachricht::sendNotification("TIMETABLE", "Der Stundenplan hat sich geändert. Schreibe @timetable für den neuen Stundenplan");
			}
		}
	}
}
?>