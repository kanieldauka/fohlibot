<?php
class Invitation extends Standard {
	static public $dbName = "bot";
	static public $tableName = "Invitations";
	static public $primaryKey = "Invitation_ID";

	public $Invitation_ID;
	public $Consignor;
	public $Receiver;
	public $Spiel_ID;
	public $Invitation;
	public $Status;
	static protected $columns = null;
	
	
	public function sendInvitation( $nickname ) 
	{
		$gameName = $this->Invitation;
		$receiver = $this->Receiver;
		$consignor = $this->Consignor;
		$this->Status = "pending";
		Nachricht::sendFromBot($receiver, "\xE2\x9D\x93 Einladung zu $gameName \xE2\x9D\x93\n$nickname hat ihnen eine einladung zu $gameName geschickt.\nSchreibe ja um sie zu akzeptieren und nein um sie abzulehnen.");		
	}
	public function pendingInvitation( $Receiver )
	{
		$invitations = Invitation::find("Receiver='".$Receiver."' AND Status='pending'");
		return array_pop( $invitations );
	}
	public function answer( $answer )
	{
		switch( strtoupper( $answer ) )
		{
			case "JA":
				$this->Status = "accepted";
				$this->Save();
				$game = Game::get( $this->Spiel_ID );
				$game->Spieler_2 = $this->Receiver;
				$game->startGame();
				
			break;
			
			case "NEIN":
				$this->Status = "declined";
				$this->Save();
				$game = Game::get( $this->Spiel_ID );
				$game->delete("Spiel_ID='" . $this->Spiel_ID . "'");
				Nachricht::sendFromBot($this->Consignor, "Deine Einladung zu $this->Invitation wurde abgelehnt");
			break;
		}
	}
}
?>