<?php
class User extends Standard{
	static public $dbName = "bot";
	static public $tableName = "User";
	static public $primaryKey = "ID";
	static protected $tableDef = null;
	public $ID;
	public $User_ID;
	public $Nickname;
	public $Rights;
	public $Class;
	public $Notices = array();
	static protected $DefaultRights = array("SETNICKNAME", "SHOWUSERS", "NOTIFY", "PLAY", "CHANGECLASS");
	static protected $columns = null;
	

	public function init($data = null) {
		parent::init($data);
		$Rights = Befugnis::find("User_ID=$this->User_ID");
		foreach($Rights as $right) {
			$this->Rights[] = $right->Befugnis;
		}

		$Notices = Notice::find("User_ID=$this->User_ID");
		foreach($Notices as $notice) {
			$this->Notices[] = $notice->Notice;
		}

	}
	public function setDefaultRights() {
		$oldRights = new Befugnis;
		$oldRights->delete("User_ID = $this->User_ID");		//Rechte resetten

		foreach($this->DefaultRights as $right) {
			$befugnis = new Befugnis;
			$befugnis->User_ID = $this->User_ID;
			$befugnis->Befugnis = $right;
			$this->Rights[] = $befugnis;
			$befugnis->save();
		}
		return true;
	}

	public function hasRights( $right ) {
		$right = strtoupper( $right );
		if(in_array($right, $this->Rights) || in_array($right, Befugnis::$defaultFunctions)) {
			return true;
		}else{
			$nachricht = new Nachricht;
			$nachricht->Consignor = $_ENV["WA_ACCOUNT_NR"];
			$nachricht->Receiver = $this->User_ID;
			$nachricht->Nachricht = "Du hast nicht die erforderlichen Rechte für diesen Befehl";
			$nachricht->send();
			return false;
		}
	}
	public function findUser( $idOrNickname ) {
		$target_user = User::find("User_ID='$idOrNickname'");		//Zieluser
		if(!$target_user)
		{
			$target_user = User::find("Nickname='".$idOrNickname."'");
			if(!$target_user)
			{
				return false;
			}
		}
		return array_pop($target_user);
	}
	public function addNotice ( $notice ) {
		$notice = strtoupper($notice);
		if(!in_array($notice, $this->Notices)) {
			$newNotice = new Notice;
			$newNotice->User_ID = $this->User_ID;
			$newNotice->Notice = $notice;
			$newNotice->save();
			return true;
		}else{
			$deleteNotice = new Notice;
			$deleteNotice->delete("`User_ID`='$this->User_ID' AND `Notice`='$notice'");
			return false;
		}

	}
	public function isAllowedToUse( $fromNumber, $befehl, $notify)
	{
		$befehl = strtoupper( $befehl );
		if($user = User::find("User_ID='" . $fromNumber . "'", false ,"1")[0])  // ist registriert ?
		{
			if($user->hasRights( $befehl ))
			{
				return $user;
			}
		}else{
			if(in_array( $befehl, Befugnis::$defaultFunctions)) // eine default Funktion kann auch von unregistrierten Benutzern aufgerufen werden
			{
				$unregistered = new User;
				$unregistered->User_ID = $fromNumber;
				$unregistered->Nickname = $notify;
				return $unregistered;
			}else{
				Nachricht::sendFromBot( $fromNumber, "Um diesen Befehl nutzen zu können registriere dich bitte mit\n\t@register");
				return false;
			}
		}
		
	}
	public function invite( $receiver, $game )
	{
		$invite = new Invitation;
		$invite->Consignor = $this->User_ID;
		$invite->Receiver = $receiver;
		$invite->Invitation = $game->SpielArt;
		$invite->Spiel_ID = $game->Spiel_ID;
		$invite->Save();
		$invite->sendInvitation($this->Nickname);
	}
	public function hasPendingInvitation( $answer )
	{
		$pendingInvite = Invitation::pendingInvitation( $this->User_ID );
		return $pendingInvite;
	}
	public function hasPendingGameTurn()
	{
		$pendingGame = Game::pendingGameTurn( $this );
		return $pendingGame;
	}
}
?>