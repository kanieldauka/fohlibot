<?php
class Nachricht extends Standard{
	static public $dbName = "bot";
	static public $tableName = "Nachrichten";
	static public $primaryKey = "Nachrichten_ID";
	public $Nachrichten_ID;
	public $Consignor;
	public $Receiver;
	public $Master;
	public $Slave;
	public $Nachricht;
	static protected $columns = null;
	
	
	public function send() {
		$_ENV["WP_connection"]->sendMessage($this->Receiver, $this->Nachricht);
		$_ENV["WP_connection"]->pollMessage();
		//echo("\t=>an $this->Receiver geantwortet: $this->Nachricht\n\n");
		logIt("Bot => $this->Receiver: $this->Nachricht");
		//$this->save();
	}
	
	public function addToQueue($connection ,$debug = false) {
		$connection->sendMessage($this->Receiver, $this->Nachricht);
		//echo("\t=>an $this->Receiver geantwortet: $this->Nachricht\n\n");
		if(false) {
			$this->save();
		}
	}
	
	public function sendNotification( $notify, $nachricht) {
		$notifiedUsers = new Notice;
		$users = $notifiedUsers->find("Notice='".$notify."'");
		
		foreach($users as $user) {
			$message = new Nachricht;
			$message->Receiver = $user->User_ID;
			$message->Nachricht = $nachricht;
			$message->Consignor = $_ENV["WA_ACCOUNT_NR"];
			$message->send();
			usleep(550000);
		}
	}
	
	public function checkAndExecCommand($notify) {
		$message_exploded = explode(" ", $this->Nachricht);
		$befehl = $message_exploded[0];
		$fromNumber = $this->Consignor;
				
		if(Befugnis::isBefugnis( $befehl )) 											//existierender Befehl?
		{
			$befehl = str_replace( array("@", "$"), array("", ""), $befehl); 			//@ oder $ entfernen
			if($consignor = User::isAllowedToUse($fromNumber, $befehl, $notify)) 		//registrierter Nutzer und befähigt zum Ausführen?
			{
				Befugnis::runCommand($consignor, $this->Nachricht);						//Befehl ausführen
			}
		}else if( $consignor = User::findUser( $fromNumber ) )
		{
			if( $invitation = $consignor->hasPendingInvitation( $befehl ) )			// Einladung offen?
			{
				$invitation->answer( $befehl );
			}else if( $game = $consignor->hasPendingGameTurn() )				//Laufender Spielzug?
			{
				$game->answerGame( $this->Nachricht );
			}else{
				
			}
		}
	}
	public function sendfromBot($receiver, $msg)
	{
		$message = new Nachricht;
		$message->Receiver = $receiver;
		$message->Nachricht = $msg;
		$message->Consignor = $_ENV["WA_ACCOUNT_NR"];
		$message->send();
	}
	
}
?>