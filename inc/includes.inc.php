<?php 
require_once(dirname(dirname(__FILE__))."/lib/whatsapp/src/whatsprot.class.php");
require_once(dirname(dirname(__FILE__))."/lib/whatsapp/src/events/MyEvents.php");
require_once(dirname(__FILE__)."/msqli.db.inc.php");
require_once(dirname(__FILE__)."/global.settings.inc.php");

require_once(dirname(__FILE__)."/Default.class.php");
require_once(dirname(__FILE__)."/Nachricht.class.php");
require_once(dirname(__FILE__)."/User.class.php");
require_once(dirname(__FILE__)."/Befugnis.class.php");
require_once(dirname(__FILE__)."/Notice.class.php");
require_once(dirname(__FILE__)."/Stundenplan.class.php");
require_once(dirname(__FILE__)."/Eigenschaft.class.php");
require_once(dirname(__FILE__)."/Stunde.class.php");
require_once(dirname(__FILE__)."/Game.class.php");
require_once(dirname(__FILE__)."/Invitation.class.php");
?>