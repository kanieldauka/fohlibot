<?php
class Eigenschaft extends Standard{
	static public $dbName = "bot";
	static public $tableName = "Eigenschaften";
	static public $primaryKey = "E_ID";
	public $name;
	public $id;
	public $type;
	public $displayname;
	public $alternatename;
	public $longName;
	public $Class;
	static protected $columns = null;
	
}
?>