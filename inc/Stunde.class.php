<?php
class Stunde extends Standard{
	static public $dbName = "bot";
	static public $tableName = "Stunden";
	static public $primaryKey = "S_ID";
	public $S_ID;
	public $id;
	public $date;
	public $startTime;
	public $endTime;
	public $eigenschaften;
	public $is;
	public $Standard;
	public $Cancelled;
	public $roomSubstitution;
	public $substitution;
	public $Class;
	public $finished;
	public $hasInfo;
	static protected $columns = null;
	


	public function init($data = null) {
		parent::init($data);
		$date = date("Ymd");
		$time = date("Hi");
		if($this->substitution) {
			$this->finished = "\xE2\x9A\xA0";
		}else if($date > $this->date) {
			$this->finished = "\xE2\x9C\x85";
		}else if($date == $this->date){
			if($time >= $this->endTime) {
				$this->finished = "\xE2\x9C\x85";
			}else{
				$this->finished = "\xF0\x9F\x95\x9E";
			}
		}else{
			$this->finished = "\xF0\x9F\x95\x9E";
		}
		if($this->Cancelled) {
			$this->finished = "\xF0\x9F\x9A\xAB";
		}
		$eigenschaften = Stundeneigenschaften::find("S_ID=$this->id");
		foreach($eigenschaften as $e) {
			$eigenschaft = Eigenschaft::find("id=$e->E_ID AND type=$e->type")[0];
			$this->eigenschaften["$eigenschaft->type"] = $eigenschaft;
		}
	}
}
class Stundeneigenschaften extends Standard{
	static public $dbName = "bot";
	static public $tableName = "Stundeneigenschaften";
	static public $primaryKey = "ID";
	public $ID;
	public $S_ID;
	public $E_ID;
	public $type;
	public $Class;
}
?>