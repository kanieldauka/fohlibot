<?php
abstract class Standard {
	
	static public $dbName;
	static public $primaryKey;
	static public $tableName;
	static public $foundRows;
	static public $joinTable;
	static public $usingKey;
	static protected $columns = null;
	
	static public $findSql = "SELECT * FROM %DB%.%TABLE% WHERE %WHERE% ORDER BY %ORDER% LIMIT %LIMIT%";
	static public $joinSql = "";
	
	static function prepareSql( $sql , $where="", $order = false, $limit = PHP_INT_MAX) {
		return str_replace( array('%WHERE%', '%LIMIT%', '%ORDER%', '%DB%', '%TABLE%'), array($where, $limit, $order ? $order : static::$primaryKey, static::$dbName, static::$tableName), $sql );
	}
	
	static function query( $sql ) {
		$class = get_called_class();
		$data = [];
		$res = $_ENV["db"]->query( $sql );
		while ($row = $res->fetch_assoc()) {
			if(strtolower($class) == 'base')
			{
				$obj = $data;
			}
			else
			{
				$obj = new $class;
				$obj->init($row);
			}
			$data[] = $obj;
		}
		return $data;
	}
	
	static function find( $where = 1, $order = false, $limit = PHP_INT_MAX ){
		$sql = static::prepareSql( static::$findSql, $where, mysql_escape_string($order), mysql_escape_string($limit));
		return static::query($sql);
	}
	
	static function get( $primaryKey ) {
		$where = static::$primaryKey . "=$primaryKey";
		$sql = static::prepareSql( static::$findSql, $where, false, "1");
		$result = static::query( $sql );
		return array_pop($result);
	}
	public function delete( $WHERE ) {
		$sql = static::prepareSql("DELETE FROM  %DB%.%TABLE% WHERE " . $WHERE);
		$res = $_ENV["db"]->query( $sql );
	}
	public function save() {
		$table = static::$dbName . '.' . static::$tableName;
		$values = new stdClass;
		$primaryKey = static::$primaryKey;
		$isNew = empty($this->$primaryKey) ? true : false;
		$command = $isNew ? "INSERT INTO" : "UPDATE";
		$where = $isNew ? "" : "WHERE " . static::$primaryKey . "='".$this->$primaryKey . "'"; 
		foreach( $this as $field => $value ){
			if(in_array($field, static::$columns)) 
			{
				if( $value == static::$primaryKey && $isNew ) continue;
				$values->$field = $this->$field;
			}
		}
		
		foreach( $values as $field => $value ){
			if( !is_null($value) && !is_array($value))
			{
				$values->$field = "$field = '" . mysql_real_escape_string( $value ) . "'";
			}else{
				unset($values->$field);
			}
		}
		$sql = "$command $table SET " . implode(",", (array)$values) . " $where";
		$res = $_ENV["db"]->query( $sql );
		if( $res ){
			if( $isNew ){
				$this->$primaryKey = $_ENV["db"]->insert_id;
			}
			$this->init();
		}
	}
	
	public function apply( $obj ) {
		$obj = (object)$obj;
		
		foreach( $this as $property => $value ){
			if ( property_exists( $obj, $property ) ){
				$this->$property = $obj->$property;
			}
		}
	}
	protected function init($data = null){
		if($data) $this->apply( $data );
	}
	public function __construct(){
		if( is_null(static::$columns) )
		{
			$sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='" . static::$dbName ."' AND TABLE_NAME='" . static::$tableName . "'";
			$res = $_ENV["db"]->query( $sql );
			$columns = array();
				while( $row = mysqli_fetch_array( $res ) )
				{
					$columns[] = $row["COLUMN_NAME"];
				}
			static::$columns = $columns;
		}
		
	}
	
}