-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 11. Mrz 2016 um 17:43
-- Server-Version: 10.1.9-MariaDB
-- PHP-Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `bot`
--
CREATE DATABASE IF NOT EXISTS `bot` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bot`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eigenschaften`
--

CREATE TABLE `eigenschaften` (
  `E_ID` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `displayname` varchar(32) NOT NULL,
  `alternatename` varchar(32) NOT NULL,
  `longName` varchar(32) NOT NULL,
  `Class` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `invitations`
--

CREATE TABLE `invitations` (
  `Invitation_ID` int(11) NOT NULL,
  `Consignor` varchar(18) NOT NULL,
  `Receiver` varchar(18) NOT NULL,
  `Spiel_ID` varchar(11) NOT NULL,
  `Invitation` text NOT NULL,
  `Status` enum('pending','accepted','declined') NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nachrichten`
--

CREATE TABLE `nachrichten` (
  `Nachrichten_ID` int(11) NOT NULL,
  `Consignor` varchar(18) NOT NULL,
  `Receiver` varchar(18) NOT NULL,
  `Master` varchar(18) NOT NULL,
  `Slave` varchar(18) NOT NULL,
  `Nachricht` text NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `notices`
--

CREATE TABLE `notices` (
  `Notice_ID` int(11) NOT NULL,
  `User_ID` varchar(18) NOT NULL,
  `Notice` varchar(18) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rights`
--

CREATE TABLE `rights` (
  `Rights_ID` int(11) NOT NULL,
  `User_ID` varchar(18) NOT NULL,
  `Befugnis` varchar(32) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spiel`
--

CREATE TABLE `spiel` (
  `Spiel_ID` int(11) NOT NULL,
  `Save` text NOT NULL,
  `Spieler_1` varchar(18) NOT NULL,
  `Spieler_2` varchar(18) NOT NULL,
  `AmZug` varchar(18) NOT NULL,
  `SpielArt` varchar(20) NOT NULL,
  `Status` enum('running','ended') NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stunden`
--

CREATE TABLE `stunden` (
  `S_ID` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `date` varchar(28) NOT NULL,
  `startTime` varchar(8) NOT NULL,
  `endTime` varchar(8) NOT NULL,
  `Standard` tinyint(1) NOT NULL,
  `hasInfo` text NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Cancelled` tinyint(1) NOT NULL,
  `roomSubstitution` tinyint(1) NOT NULL,
  `substitution` tinyint(1) NOT NULL,
  `Class` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stundeneigenschaften`
--

CREATE TABLE `stundeneigenschaften` (
  `ID` int(11) NOT NULL,
  `S_ID` int(11) NOT NULL,
  `E_ID` int(11) NOT NULL,
  `type` int(4) NOT NULL,
  `Class` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `User_ID` varchar(18) NOT NULL,
  `Nickname` varchar(32) NOT NULL,
  `Class` varchar(8) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `eigenschaften`
--
ALTER TABLE `eigenschaften`
  ADD PRIMARY KEY (`E_ID`);

--
-- Indizes für die Tabelle `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`Invitation_ID`);

--
-- Indizes für die Tabelle `nachrichten`
--
ALTER TABLE `nachrichten`
  ADD PRIMARY KEY (`Nachrichten_ID`);

--
-- Indizes für die Tabelle `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`Notice_ID`);

--
-- Indizes für die Tabelle `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`Rights_ID`);

--
-- Indizes für die Tabelle `spiel`
--
ALTER TABLE `spiel`
  ADD PRIMARY KEY (`Spiel_ID`);

--
-- Indizes für die Tabelle `stunden`
--
ALTER TABLE `stunden`
  ADD PRIMARY KEY (`S_ID`);

--
-- Indizes für die Tabelle `stundeneigenschaften`
--
ALTER TABLE `stundeneigenschaften`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `eigenschaften`
--
ALTER TABLE `eigenschaften`
  MODIFY `E_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `invitations`
--
ALTER TABLE `invitations`
  MODIFY `Invitation_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `nachrichten`
--
ALTER TABLE `nachrichten`
  MODIFY `Nachrichten_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `notices`
--
ALTER TABLE `notices`
  MODIFY `Notice_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `rights`
--
ALTER TABLE `rights`
  MODIFY `Rights_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `spiel`
--
ALTER TABLE `spiel`
  MODIFY `Spiel_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `stunden`
--
ALTER TABLE `stunden`
  MODIFY `S_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `stundeneigenschaften`
--
ALTER TABLE `stundeneigenschaften`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
