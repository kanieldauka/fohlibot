<?php
set_time_limit(25);

require_once("inc/includes.inc.php");

//Change to your time zone
date_default_timezone_set('Europe/Madrid');
$debug = false;  

$w = new WhatsProt($_ENV["WA_ACCOUNT_NR"], $_ENV["WA_BOT_NAME"], $debug);
$w->connect(); // Connect to WhatsApp network
$w->loginWithPassword($_ENV["WA_ACCOUNT_PW"]);
$_ENV["WP_connection"] = $w;

for($i=0;$i<5;$i++)	//Online gehen und Nachrichten empfangen , auch für Nachrichten senden notwendig
{
	$w->pollMessage();
}
$msgs = $w->GetMessages();

Stundenplan::updateIfNeeded();

foreach ($msgs as $m) {
	$silent = false;
	$fromNumber = explode("@", $m->getAttribute("from"))[0];
	$message = $m->getChild("body")->getData();
	$notify = $m->getAttribute("notify");	//Nickname des Versenders
	//echo("Nachricht von $notify, am ".date("d.m.Y \u\m H:i:s").",($fromNumber): $message\n");
	logIt("$notify($fromNumber): $message");
	$incomingMessage = new Nachricht;   //einfallende Nachricht
	$incomingMessage->Consignor = $fromNumber;
	$incomingMessage->Receiver = $_ENV["WA_BOT_NAME"];
	$incomingMessage->Nachricht = $message;
	//$incomingMessage->save();
	
	$incomingMessage->checkAndExecCommand($notify);
}

?>
